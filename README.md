# About the project

**Example Paint** is an example for [Advanced Android in Kotlin 02.2: Drawing on canvas objects](https://codelabs.developers.google.com/codelabs/advanced-android-kotlin-training-canvas/). It extends the Mini-Paint app from the codelab with the palette widget (16-color EGA palette), which allows to pick the drawing color, and a slider widget (Material Slider) to pick the line thickness.

It was surprisingly easy and fulfilling. The palette widget is adapted from my previous exercise, [Custom Numpad](https://bitbucket.org/catness/custom_numpad), which uses the same approach: a custom view with different clickable areas.

The apk is here: [paint.apk](apk/paint.apk).
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)
