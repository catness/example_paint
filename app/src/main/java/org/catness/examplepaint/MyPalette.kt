package org.catness.examplepaint

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PointF
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View

class MyPalette @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
    private val LOG_TAG: String = this.javaClass.simpleName
    private var cellWidth = 0f
    private var cellHeight = 0f
    private val radius = 10f

    private val ega: List<String> = listOf(
        "#000000", "#0000aa", "#00aa00", "#00aaaa", "#aa0000", "#aa00aa", "#aa5500", "#aaaaaa",
        "#555555", "#5555ff", "#55ff55", "#55ffff", "#ff5555", "#ff55ff", "#ffff55", "#ffffff"
    )
    private val colors: List<Int> = ega.map { Color.parseColor(it) }
    private var selected = 0
    private var _selectedColor: Int = colors[selected]
    var selectedColor: Int = colors[selected]
        get() = _selectedColor


    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
    }

    init {
        isClickable = true
    }

    override fun performClick(): Boolean {
        // The call to super.performClick() must happen first,
        // which enables accessibility events as well as calls onClickListener().
        Log.i(LOG_TAG, "performClick ")
        if (super.performClick()) return true
        invalidate()
        return true
    }

    fun processMyClick(pointF: PointF) {
        Log.i(LOG_TAG, "process my click...")
        var x = pointF.x
        // var y = pointF.y
        selected = (x / cellWidth).toInt()
        _selectedColor = colors.getOrElse(selected, { colors[0] })
        Log.i(LOG_TAG, "x=$x selected num=$selected color=$_selectedColor")
        selectedColor = _selectedColor
        invalidate()
    }

    // The onSizeChanged() method is called any time the view's size changes,
    // including the first time it is drawn when the layout is inflated.
    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        cellWidth = (width / colors.size).toFloat()
        cellHeight = height.toFloat()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val y = 0f
        var col = 0
        colors.forEach {
            var x: Float = cellWidth * col
            paint.color = it
            canvas.drawRect(x, y, x + cellWidth, y + cellHeight, paint)
            if (col == selected) {
                paint.color = when (col) {
                    0, 1, 4, 5, 6, 8, 9 -> Color.WHITE
                    else -> Color.BLACK
                }
                Log.i(LOG_TAG, "selected: $col radius=$radius $x $y")
                canvas.drawCircle(x + cellWidth / 2, y + cellHeight / 2, radius, paint)
            }
            col++
        }
    }

}

// https://stackoverflow.com/questions/1967039/onclicklistener-x-y-location-of-event
@SuppressLint("ClickableViewAccessibility")
fun View.setOnClickListenerWithPoint(action: (PointF) -> Unit) {
    val coordinates = PointF()
    //val screenPosition = IntArray(2)
    setOnTouchListener { v, event ->
        Log.i("Palette", "listener")
        if (event.action == MotionEvent.ACTION_DOWN) {
            //v.getLocationOnScreen(screenPosition)
            //coordinates.set(event.x.toInt() + screenPosition[0], event.y.toInt() + screenPosition[1])
            coordinates.set(event.x, event.y)
        }
        false
    }
    setOnClickListener {
        action.invoke(coordinates)
        Log.i("Palette", "coords = $coordinates")
    }
}

