package org.catness.examplepaint

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.android.material.slider.Slider

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mypalette: MyPalette = findViewById(R.id.palette)
        val mycanvas: MyCanvasView = findViewById(R.id.canvas)
        val slider: Slider = findViewById(R.id.slider)
        mycanvas.setColor(mypalette.selectedColor)

        mypalette.setOnClickListenerWithPoint {
            //Toast.makeText(this, "Click $it", Toast.LENGTH_LONG).show()
            Log.d("Main", "click on palette $it")
            mypalette.processMyClick(it)
            mycanvas.setColor(mypalette.selectedColor)
        }
        slider.addOnChangeListener { slider, value, fromUser ->
            Log.d("Main", "value=$value")
            mycanvas.setStrokeWidth(value)
        }

    }
}